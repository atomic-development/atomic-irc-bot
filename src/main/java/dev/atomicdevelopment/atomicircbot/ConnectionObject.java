package dev.atomicdevelopment.atomicircbot;

public class ConnectionObject {

    public String ip;
    public int port;
    public boolean ssl;
    public int timeout;  // connection timeout in seconds
}
