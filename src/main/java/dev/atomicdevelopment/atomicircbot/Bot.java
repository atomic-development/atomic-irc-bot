package dev.atomicdevelopment.atomicircbot;

import com.moandjiezana.toml.Toml;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLSocketFactory;
import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;

public class Bot {

    private static final Logger logger = LoggerFactory.getLogger(Bot.class);
    public static Toml toml;
    public static BufferedWriter output;
    public static BufferedReader input;

    private static void generateConfig() {
        try {
            InputStream defaultConfig = Bot.class.getResourceAsStream("/config.toml");
            assert defaultConfig != null;
            Files.copy(defaultConfig, Path.of("config.toml"));
        } catch (IOException error) {
            error.printStackTrace();
        }
        logger.error("Config could not be found, a new one has been generated for you, exiting...");
        System.exit(1);
    }

    public static void send(String ircCommand, String message) {
        try {
            String msg = String.format("%s %s\r\n", ircCommand, message);
            output.write(msg);
            output.flush();
            logger.info("Sent: " + msg);
        } catch (IOException error) {
            error.printStackTrace();
        }
    }

//    public static void send(String ircCommand) {
//        try {
//            output.write(ircCommand + "\r\n");
//            output.flush();
//        } catch (IOException error) {
//            error.printStackTrace();
//        }
//    }

    public static void main(String[] args) {

        // configuration checking

        try {
            toml = new Toml().read(new FileInputStream("config.toml"));
        } catch (IOException error) {
            generateConfig();
        }

        // attempt to connect to irc

        ConnectionObject connectionObject = toml.getTable("Connection").to(ConnectionObject.class);
        try {
            Socket socket;
            if (connectionObject.ssl) {
                socket = SSLSocketFactory.getDefault().createSocket();
            } else {
                socket = new Socket();
            }
            InetSocketAddress address = new InetSocketAddress(connectionObject.ip, connectionObject.port);
            if (connectionObject.timeout == 0) {
                logger.warn("No timeout has been passed, connection attempt may be indefinite");
            }
            socket.connect(address, connectionObject.timeout * 1000);

            output = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            input = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            String name = toml.getString("nickname");
            if (name == null || name.length() == 0) {
                logger.error("Could not find username in config file, exiting...");
                System.exit(1);
            }

            // Authentication

            send("USER", String.format("%1$s 0 * :%1$s", name));
            send("NICK", name);

            while (true) {
                String msg = input.readLine();
                if (msg == null) {
                    break;
                }
                System.out.println(msg);
                int spaceIndex = msg.indexOf(" ");
                if (msg.substring(0, spaceIndex).equals("PING")) {
                    String code = msg.substring(spaceIndex + 1);
                    send("PONG", code);
                }
                int secondSpaceIndex = msg.indexOf(" ", spaceIndex + 1);
                if (secondSpaceIndex == -1) {
                    continue;
                }
                String code = msg.substring(spaceIndex + 1, secondSpaceIndex);
                CodeHandler.handleCode(code);
            }

        } catch (IOException error) {
            logger.error(String.format("Failed to connect to server in %d seconds, is the connection valid?", connectionObject.timeout));
            logger.info("If the credentials are correct, you could be rate limited");
            System.exit(1);
        }
    }
}
