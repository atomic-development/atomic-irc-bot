package dev.atomicdevelopment.atomicircbot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import static dev.atomicdevelopment.atomicircbot.Bot.send;
import static dev.atomicdevelopment.atomicircbot.Bot.toml;

public class CodeHandler {

    private static final Logger logger = LoggerFactory.getLogger(CodeHandler.class);

    public static void handleCode(String code) {

        switch (code) {
            case "001":
                List<Object> channels = toml.getList("channels");
                if (channels.size() == 0) {
                    logger.warn("No channels specified in the config file, the bot will not reply to commands");
                }
                for (Object channel : channels) {
                    send("JOIN", channel.toString());
                }
                break;

            default:
                break;
        }
    }
}
